<?php
/**
 * @package   UWM HTML POST to Shortcode
 * @author    Catarino David Delgado <delgadcd@uwm.edu>
 * @license   MIT
 * @link      https://bitbucket.org/uwmcetl/uwm-form-post-to-session/src/
 * @copyright 2020 University of Wiscosnin-Milwaukee
 *
 * @wordpress-plugin
 * Plugin Name: UWM HTML POST to Shortcode
 * Plugin URI:  https://bitbucket.org/uwmcetl/uwm-form-post-to-session/src/
 * Description: Access an HTML POST in WordPress using shortcodes
 * Version:     1.1.4
 * Author:      Catarino David Delgado <delgadcd@uwm.edu>
 * Author URI:  https://uwm.edu/cetl/people/delgado-david/
 * License:     MIT
 * License URI: https://bitbucket.org/uwmcetl/uwm-form-post-to-session/src/master/license.txt
 */


 // If not in WordPress, don't execute code
if ( !defined( 'ABSPATH' ) ) exit;


// Constants
define( 'UHPTS_SESSION', 'uhpts_818df' );
define( 'UHPTS_PATH', '/' );
define( 'UHPTS_TIMEOUT', 0 );


// Globals
$uhpts_post_data = array();


/**
 * Start a session if one doesn't exist.
 */
function uhpts_start_session()
{

    global $uhpts_post_data; 

    $cookie_params = array(
        'lifetime'  => UHPTS_TIMEOUT,
        'path'      => '/',
        'domain'    => '.' . $_SERVER['HTTP_HOST'],
        'secure'    => TRUE,
        'httponly'  => TRUE,
        'samesite'  => 'None'
    );
    
    if ( PHP_VERSION_ID < 70300 )
    {
        session_set_cookie_params( 
            $cookie_params['lifetime'], 
            $cookie_params['path'] . '; samesite=' . $cookie_params['samesite'],
            $cookie_params['domain'], 
            $cookie_params['secure'], 
            $cookie_params['httponly'] 
        );
    } 
    else 
    {
        session_set_cookie_params( $cookie_params );
    }

    session_start();

    if ( isset( $_POST ) && count( $_POST ) !== 0 )
    {

        $_SESSION[ UHPTS_SESSION ] = $_POST;
        $uhpts_post_data = $_POST;

    }

    session_write_close();

}
add_action( 'init', 'uhpts_start_session', 1 );


/**
 * Shortcode to load post data into session
 * 
 * @param array $atts
 * @param string $content
 * 
 * @return string Either processed content if attributes work, or an empty string.
 */
function uhpts_load_data( $atts, $content = null )
{

    global $uhpts_post_data;

    $response = '';

    // Set tag attributes and defaults
    $tag_defaults = array(
        'origin'    =>  '*',
        'values'    =>  null
    );
    $params = shortcode_atts( $tag_defaults, $atts);
    
    $same_site_origin = strpos( $_SERVER[ 'HTTP_REFERER' ], $_SERVER['HTTP_HOST'] ) !== false;
    if ( !$same_site_origin && $params[ 'origin' ] !== '*' )
    {
       
        // Security is of importance, do a referrer check
        $origin_mask = '/' . $params[ 'origin' ] . '/i';
        if ( preg_match( $origin_mask, $_SERVER[ 'HTTP_REFERER' ] ) !== 1 )
        {
            return $response;
        } 
        
    }

    if ( isset( $_SESSION[ UHPTS_SESSION ] ) && count( $uhpts_post_data ) !== 0 )
    {

        if ( $params[ 'values' ] )
        {
         
            // Only keep the desired values from $_POST
            $keys = explode( ',', str_replace( ' ', '', $params[ 'values' ] ) ); 
            foreach ( $keys as $key )
            {

                if ( !array_key_exists( $key, $uhpts_post_data ) )
                {
                    return $response;
                }
                
            }

        }

    }

    if ( !empty( $content ) ) 
    {
        $response = apply_filters( 'the_content', $content );
    }

    return $response;
    
}
add_shortcode( 'html_load_post', 'uhpts_load_data' );


/**
 * Return a sanitized value from the session
 * 
 * @param array $atts
 * 
 * @return string - filtered value of the requested parameter or empty string if it doesn't exist.
 */
function uhpts_post_var_val( $atts )
{

    // If all else fails, return blank.
    $response = '';

    // Set tag attributes and defaults
    $tag_defaults = array(
        'parameter'     =>  '',
        'filter'        =>  'text',
        'htmlescape'    => false
    );
    $params = shortcode_atts( $tag_defaults, $atts);

    // Map tag filters with PHP filters
    $filters = array(
        'text'          =>  FILTER_SANITIZE_STRING,
        'email'         =>  FILTER_SANITIZE_EMAIL,
        'url-encoded'   =>  FILTER_SANITIZE_ENCODED,
        'float'         =>  FILTER_SANITIZE_NUMBER_FLOAT,
        'integer'       =>  FILTER_SANITIZE_NUMBER_INT,
        'url'           =>  FILTER_SANITIZE_URL,
        'unclean'       =>  FILTER_UNSAFE_RAW
    );

    $data = get_uhpts_data();

    // If the filter is set to something useful, and the requested post parameter exists, return the
    // properly filtered value of the property.
    if ( isset( $filters[ $params[ 'filter' ] ] ) && isset( $data[ $params[ 'parameter' ] ] ) )
    {
        $response = filter_var( $data[ $params[ 'parameter' ] ], $filters[ $params[ 'filter' ] ] );
    }

    if ( $params['htmlescape'] )
    {
        $resposne = htmlspecialchars( $response );
    }
    return $response;

}
add_shortcode( 'html_post_var_value', 'uhpts_post_var_val' );


/**
 * Check if the specified parameter exists in the session
 * 
 * @param array $atts
 * 
 * @return int - 1 if variable exists, 0 otherwise.
 */
function uhpts_post_var_exists( $atts )
{

    // If all else fails, return blank.
    $response = 0;

    // Set tag attributes and defaults
    $tag_defaults = array(
        'parameter' =>  '',
        'not'       =>  0
    );
    $params = shortcode_atts( $tag_defaults, $atts);

    $data = get_uhpts_data();

    // if the parameter exists (even if empty), return true, or false if not.
    $response = isset( $data[ $params[ 'parameter' ] ] );

    if ( $params[ 'not' ] )
    {
        $response = !$response;
    }

    return (bool) $response;

}
add_shortcode( 'html_post_var_exists', 'uhpts_post_var_exists' );


/**
 * If a parameter exists, show content
 * 
 * @param array $atts
 * @param string $content = null
 * 
 * @return string - null, or value of $content
 */
function uhpts_post_var_is( $atts, $content = null )
{
    // If all else fails, return blank.
    $response = '';

    // Set tag attributes and defaults
    $tag_defaults = array(
        'parameter' =>  '',
        'value'     =>  '',
        'not'       =>  0
    );
    $params = shortcode_atts( $tag_defaults, $atts);

    $data = get_uhpts_data();
    $param_equals_value = ( strtolower( $data[ $params[ 'parameter' ] ] ) == strtolower ( $params[ 'value' ] ) );

    if ( $params[ 'not' ] )
    {
        $param_equals_value = !$param_equals_value;
    }

    if ( $param_equals_value )
    {
        $response = apply_filters( 'the_content', $content );
    }

    return $response;

}
add_shortcode( 'html_post_var_is', 'uhpts_post_var_is' );


/**
 * Check if the parameter contains a value
 * 
 * @param array $atts
 * 
 * @return int - 1 if a match, 0 otherwise
 */
function uhpts_post_var_has( $atts )
{
    // If all else fails, return blank.
    $response = 0;

    // Set tag attributes and defaults
    $tag_defaults = array(
        'parameter' =>  '',
        'has'       =>  '_',
        'not'       =>  0
    );
    $params = shortcode_atts( $tag_defaults, $atts);

    $data = get_uhpts_data();

    // if the parameter exists, 
    if ( isset( $data[ $params[ 'parameter' ] ] ) )
    {

        $parammeter_value = strtolower( $data[ $params[ 'parameter' ] ] );
        $has_value = strtolower( $params[ 'has' ] );

        if ( $params[ 'not' ] )
        {
            $response = ( strpos( $parammeter_value, $has_value ) === false );
        }
        else
        {
            $response = !( strpos( $parammeter_value, $has_value ) === false );
        }

    }

    return (int) $response;
}
add_shortcode( 'html_post_var_has', 'uhpts_post_var_has' );

/**
 * Show content if the parameter contains a value
 * 
 * @param array $atts
 * @param string $content = null
 * 
 * @return string - $content is returned if a match, an empty string otherwise.
 */
function uhpts_post_var_show_if( $atts, $content = null )
{
    // If all else fails, return blank.
    $response = '';

    // Set tag attributes and defaults
    $tag_defaults = array(
        'parameter' =>  '',
        'has'       =>  '_',
        'not'       =>  0
    );
    $params = shortcode_atts( $tag_defaults, $atts);

    $data = get_uhpts_data();
    $test = 0;

    // if the parameter exists, 
    if ( isset( $data[ $params[ 'parameter' ] ] ) )
    {

        $parammeter_value = strtolower( $data[ $params[ 'parameter' ] ] );
        $has_value = strtolower( $params[ 'has' ] );
        $test = (strpos( $parammeter_value, $has_value ) !== false);

        if ( $params[ 'not' ] )
        {
            $test = !$test;
        }

    }

    if ( $test )
    {
        $response = apply_filters( 'the_content', $content );
    }

    return  $response;

}
add_shortcode( 'html_post_show_if', 'uhpts_post_var_show_if' );


/**
 * Display stored information as JSON
 * 
 * @return string - a JSON object wrapped in a PRE HTML tag
 */
function uhpts_to_json()
{

    $response = '';

    $data = get_uhpts_data();

    $response .= '<pre>' . PHP_EOL;
    $response .= json_encode( $data, JSON_PRETTY_PRINT );
    $response .= '</pre>' . PHP_EOL;

    return $response;

}
add_shortcode( 'html_post_json', 'uhpts_to_json' );


/**
 * Get saved post data
 * 
 * @return mixed
 */
function get_uhpts_data()
{
    //return unserialize( base64_decode( $_SESSION[ UHPTS_SESSION ] ) );
    return $_SESSION[ UHPTS_SESSION ];
}