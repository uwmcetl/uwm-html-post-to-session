# UWM HTML Post to Shortcode Plugin #

This plugin allows WordPress to use information posted to a page via HTML POST. If a plugin supports shortcodes as input, then POST values can be accessed.

## How to Install ##

* Go to the Downloads page of this repository and save a zip. Then, [upload the plugin to Wordpress](https://wordpress.org/support/article/managing-plugins/#manual-upload-via-wordpress-admin) and activate.

## How to use ##

Once activated, the following shortcodes will be available. 

On any published page which will be the target of an HTML POST

    [html_load_post]                                            // Load POST data

    [html_load_post]Content[/html_load_post]                    // Show content when load is complete (recommended)

    [html_load_post origin='uwm.edu']                           // Only accept posts from uwm.edu domains (can be easily spoofed)
        Content
    [/html_load_post]

    [html_load_post origin='uwm.edu' values='email']            // Only store the 'email' parameter from data posted from uwm.edu
        Content                                                 // Using both parameters is highly recommended to reduce data injection risk
    [/html_load_post]

After the HTML Post Load tag is used, use any of the HTML Post tags. The tags are:

### html_post_var_value ###

    [html_post_var_value parameter='age' filter='integer']      // outputs the posted praramter "age" while using the "integer" filter.

    [html_post_var_value parameter='useremail' filter='email']  // outputs a cleaned email that was posted.

    [html_post_var_value parameter='useremail' filter='email' htmlescape='1']  
                                                                // return the value filtered as an email, and also html escaped
                                                                // See https://www.php.net/manual/en/function.htmlspecialchars.php

Possible values for filter (and their PHP values--):

* text - FILTER_SANITIZE_STRING 
* email - FILTER_SANITIZE_EMAIL,
* url-encoded - FILTER_SANITIZE_ENCODED,
* float - FILTER_SANITIZE_NUMBER_FLOAT,
* integer - FILTER_SANITIZE_NUMBER_INT,
* url - FILTER_SANITIZE_URL,
* unclean - FILTER_UNSAFE_RAW

For information on the filters and what they do, refer to [PHP documentation](https://www.php.net/manual/en/filter.filters.sanitize.php).

### html_post_var_exists ###

    [html_post_var_exists parameter='age']                      // 1 if parameter exists, 0 otherwise.

    [html_post_var_exists parameter='age' not='1']              // 0 if parameter exists, 1 otherwise.

### html_post_var_is ###

    [html_post_var_is parameter='age' value='50']               // Show "Content here" if age equals 50.
        Content here
    [/html_post_var_is]

    [html_post_var_is parameter='age' value='50' not='1']       // Show "Content here" if age is not 50.
        Content here
    [/html_post_var_is]

### html_post_var_has ###

    [html_post_var_has parameter='email' has='.edu']             // 1 if email has ".edu", 0 otherwise.

    [html_post_var_has parameter='email' has='.edu' not='1']     // 0 if email does not have ".edu", 1 otherwise.

### html_post_show_if ###

    [html_post_show_if parameter='email' has='.edu']             // Show "Content here" if email has ".edu" in it.
        Content here
    [/html_post_show_if]

    [html_post_show_if parameter='email' has='.edu' not='1']     // Show "Content here" if email does not have ".edu" in it.
        Content here
    [/html_post_show_if]

### html_post_json ###

Shows all data currently saved as JSON, wrapped in a PRE tag.

## Support ##

* Email the author (delgadcd@uwm.edu) to report an issue.